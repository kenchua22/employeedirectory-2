/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory;

import javax.inject.Inject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.headspring.empdirectory.controller.EmployeeController;
import com.headspring.empdirectory.model.Employee;
import com.headspring.empdirectory.service.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/empdirectory-context-test.xml"})
public class EmployeePagesTest {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeePagesTest.class);
	
	@Inject
	private EmployeeService employeeService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Assert.assertNotNull(employeeService);
	}

	@Test
	public void testEmployeePages() {
		int pageNumber = 1; 
		int totalEmployeesCount;
		int pageSize;
		int totalPages = 0;
		
		Page<Employee> page = employeeService.getAllEmployees(pageNumber);
		Assert.assertNotNull(page);
		
		totalEmployeesCount = page.getNumberOfElements();
		logger.info("totalEmployeesCount = " + totalEmployeesCount);
		
		pageSize = page.getSize();
		logger.info("pageSize = " + pageSize);
		
		totalPages = page.getTotalPages();
		logger.info("totalPages = " + totalPages);
	
		Assert.assertTrue(totalEmployeesCount / pageSize <= totalPages);
	}

	@After
	public void tearDown() throws Exception {
	}

}
