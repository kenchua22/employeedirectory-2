/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.exception;

public class EmployeeNotFound extends Exception {
	public EmployeeNotFound() {
		super();
	}
	
	public EmployeeNotFound(String message) {
		super(message);
	}
}
