/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Employee Entity bean.
 * @author ken
 *
 */
@Entity
@Table(name="EMPLOYEE")
public class Employee {

    @Id
    @Column(name="emp_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    
    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;
    
    @Column(name="title")
    private String title;
    
    @Column(name="location")
    private String location;
    
    @Column(name="email")
    private String email;
    
    @OneToMany(mappedBy="employee", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
    @OrderBy("phoneId")
    @JsonManagedReference
    private List<EmployeeNumber> numbers;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public List<EmployeeNumber> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<EmployeeNumber> numbers) {
		this.numbers = numbers;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
