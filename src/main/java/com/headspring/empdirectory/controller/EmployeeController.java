/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.headspring.empdirectory.exception.EmployeeNotFound;
import com.headspring.empdirectory.model.Employee;
import com.headspring.empdirectory.service.EmployeeService;

@Controller
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	
	@Inject
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/pages/{pageNumber}", method = RequestMethod.GET)
	public String getEmployeesPage(@PathVariable Integer pageNumber, Model model) {

		logger.info("Finding all employees.");
	    Page<Employee> page = employeeService.getAllEmployees(pageNumber);
 
	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + 10, page.getTotalPages());

	    model.addAttribute("page", page);
	    model.addAttribute("currentIndex", current);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);

	    logger.info("Page " + current + " has " + page.getNumberOfElements() + " employees.");
	    
	    model.addAttribute("employees", page.getContent());
	    
	    return "employees";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	@ResponseBody
	public String deleteEmployee(@PathVariable Long id, Model model) throws EmployeeNotFound {
		Employee employee = employeeService.deleteEmployee(id);
		logger.info("Employee " + id + " was successfully deleted.");

		if (id != null) {
			return id.toString();
		} else {
			return "1";
		}
	}

    @RequestMapping(value="/add", method=RequestMethod.GET)
    public ModelAndView addNewEmployee() {
		ModelAndView mv = new ModelAndView("employee-form");
		mv.addObject("formMode", "add");
		mv.addObject("success", "false");
		return mv;
    }
    
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView saveNewEmployee(@ModelAttribute Employee employee) {
		ModelAndView mv = new ModelAndView();
		employeeService.createEmployee(employee);
		logger.info("Employee " + employee.getFirstName() + " " + employee.getLastName() + " was successfully created.");
		mv.setViewName("employee-form"); 
		mv.addObject("success", "true");
		mv.addObject("formMode", "add");
		return mv;    
	}
	 
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editEmployee(@PathVariable long id) {
		ModelAndView mv = new ModelAndView("employee-form");
		Employee employee = employeeService.getEmployee(id);
		logger.info("Returning employee " + employee.getId() + " for editing.");
		mv.addObject("employee", employee);
		mv.addObject("formMode", "edit");
		mv.addObject("success", "false");
		return mv;
    }

	@RequestMapping(value="/update", method=RequestMethod.POST)
	public ModelAndView updateEmployee(@ModelAttribute Employee employee) throws EmployeeNotFound {
		ModelAndView mv = new ModelAndView();
		Employee updatedEmployee = employeeService.updateEmployee(employee);
		logger.info("Employee " + employee.getId() + " was successfully updated.");
		mv.addObject("employee", updatedEmployee);
		mv.addObject("formMode", "edit");
		mv.addObject("success", "true");
		mv.setViewName("employee-form");  
		return mv;    
	}

	@RequestMapping(value="/cancel/{id}", method=RequestMethod.GET)
	@ResponseBody
	public String cancelEdit(@PathVariable Long id) {
		logger.info("Cancelling edit of employee " + id);
		return "/empdirectory2/pages/1";	//TODO: redirect to the proper paginated page
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String findEmployees(@RequestParam("term") String searchTerm, @RequestParam("page") int pageNumber, Model model) {
		logger.info("Finding all employees with search term " + searchTerm);
		
		if (searchTerm == null || searchTerm.trim().length() == 0) {
			return  getEmployeesPage(pageNumber, model);
		}
		
	    Page<Employee> page = employeeService.search(searchTerm, pageNumber);
	    
	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + 10, page.getTotalPages());

	    model.addAttribute("page", page);
	    model.addAttribute("currentIndex", current);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("searchTerm", searchTerm);
	    
	    logger.info("Page " + current + " has " + page.getNumberOfElements() + " employees.");
	    
	    model.addAttribute("employees", page.getContent());
	    
	    return "employees";
	}
	
}
