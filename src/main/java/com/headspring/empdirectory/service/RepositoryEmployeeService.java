/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.headspring.empdirectory.exception.EmployeeNotFound;
import com.headspring.empdirectory.model.Employee;
import com.headspring.empdirectory.repository.EmployeeRepository;

/**
 * This implementation of the EmployeeService interface delegates all database operations 
 * to the EmployeeRepository Spring Data JPA repository.
 * 
 * @author ken
 */

@Service
public class RepositoryEmployeeService implements EmployeeService {
	
	private static final Logger logger = LoggerFactory.getLogger(RepositoryEmployeeService.class);
	private static final int PAGE_SIZE = 300;

	@Inject
	private EmployeeRepository employeeRepository;

	@Transactional
    public Page<Employee> getAllEmployees(Integer pageNumber) {
    	
    	// Spring Data uses 0-indexed pages, but the pageNumber parameter will be 1-indexed based so as to be user friendly
        PageRequest pageRequest = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "id");
        
        logger.info("Finding all employees on page " + pageNumber);
        return employeeRepository.findAll(pageRequest);
    }

	@Transactional(rollbackFor=EmployeeNotFound.class)
	public Employee deleteEmployee(Long id) throws EmployeeNotFound {
		logger.info("Finding employee " + id);
		Employee employee = employeeRepository.findOne(id);
		
		if (employee == null) {
			logger.error("Employee " + id + " not found!");
			throw new EmployeeNotFound("Employee " + id + " not found!");
		}
		
		logger.info("Deleting employee " + id);
		employeeRepository.delete(employee);
		return employee;
	}

	@Transactional
	public Employee createEmployee(Employee employee) {
		logger.info("Creating employee "+ employee.getFirstName() + " " + employee.getLastName());
		return employeeRepository.save(employee);
	}

	@Transactional
	public Employee getEmployee(Long id) {
		logger.info("Finding employee " + id);
		return employeeRepository.findOne(id);
	}
	
	@Transactional(rollbackFor=EmployeeNotFound.class)
	public Employee updateEmployee(Employee employee) throws EmployeeNotFound {
		logger.info("Finding employee " + employee.getId());
		Employee dbEmployee = employeeRepository.findOne(employee.getId());
		
		if (dbEmployee == null) {
			logger.error("Employee " + employee.getId() + " not found!");
			throw new EmployeeNotFound("Employee " + employee.getId() + " not found!");
		}

		if (employee != null && employee.getNumbers() == null) {
			if (dbEmployee.getNumbers() != null) {
				dbEmployee.getNumbers().clear();
			}
		}
		logger.info("Updating employee " + employee.getId());
		return employeeRepository.save(dbEmployee);
	}

	public Page<Employee> search(String searchTerm, Integer pageNumber) {
    	
    	// Spring Data uses 0-indexed pages, but the pageNumber parameter will be 1-indexed based so as to be user friendly
        PageRequest pageRequest = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "id");
        
        logger.info("Finding all employees with " + searchTerm + " on page " + pageNumber);
        return employeeRepository.findEmployees("%" + searchTerm + "%", pageRequest);
	}

}
