/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.service;

import org.springframework.data.domain.Page;

import com.headspring.empdirectory.exception.EmployeeNotFound;
import com.headspring.empdirectory.model.Employee;

public interface EmployeeService {
	public Page<Employee> getAllEmployees(Integer pageNumber);
	public Employee deleteEmployee(Long id) throws EmployeeNotFound;
	public Employee createEmployee(Employee employee);
	public Employee getEmployee(Long id);
	public Employee updateEmployee(Employee employee) throws EmployeeNotFound;
	public Page<Employee> search(String searchTerm, Integer pageNumber);
}
