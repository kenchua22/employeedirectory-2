/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.headspring.empdirectory.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("SELECT emp FROM com.headspring.empdirectory.model.Employee emp WHERE LOWER(emp.firstName) LIKE LOWER(:searchTerm) OR LOWER(emp.lastName) LIKE LOWER(:searchTerm) OR LOWER(emp.title) LIKE LOWER(:searchTerm) OR LOWER(emp.location) LIKE LOWER(:searchTerm) OR LOWER(emp.email) LIKE LOWER(:searchTerm) ")
	Page<Employee> findEmployees(@Param("searchTerm") String searchTerm, Pageable pageable);
}
