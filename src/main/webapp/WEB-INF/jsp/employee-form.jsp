<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">

<head>
	<c:if test="${formMode == 'edit'}">
		<title>Employee Directory App - Edit Employee</title>
	</c:if>
	<c:if test="${formMode == 'add'}">
		<title>Employee Directory App - Add New Employee</title>
	</c:if>

	<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootbox.js"></script>
	<script src="${pageContext.request.contextPath}/js/app.js"></script>

	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
	
</head>

<body>
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
			
			</div>
		</div>
				
		<br>

		<c:if test="${formMode == 'edit'}">
			<h2>Edit Employee</h2>
		</c:if>
		<c:if test="${formMode == 'add'}">
			<h2>Add New Employee</h2>
		</c:if>
		
		<form name="empForm" novalidate">

		<div class="row">
			<div class="col-xs-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Employee Details</h3>
					</div>
					<div class="panel-body">
						<c:if test="${(formMode == 'edit') && (success == 'true')}">
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								Employee successfully updated.
							</div>
						</c:if>
						<c:if test="${(formMode == 'add') && (success == 'true')}">
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								Employee successfully created.
							</div>
						</c:if>
								      
						<div class="fields">
							<c:if test="${formMode == 'edit'}">
								<label class="control-label">Employee ID</label>
								<span>${employee.id}</span>
								<input type="hidden" id="id" name="id" value="${employee.id}"/>
							</c:if>
							
							<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label">First Name</label>
								<input type="text" class="form-control" name="firstName" value="${employee.firstName}" required placeholder="First Name" />
								<p class="help-block">First Name is required</p>
							</div>
					
							<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label">Last Name</label>
								<input type="text" class="form-control" name="lastName" value="${employee.lastName}" required placeholder="Last Name" />
								<p class="help-block">Last Name is required</p>
							</div>
							      
					 		<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label">Title</label>
								<input type="text" class="form-control" name="title" value="${employee.title}" required placeholder="Title" />
								<p class="help-block">Title is required</p>
							</div>
					
					 		<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label">Location</label>
								<input type="text" class="form-control" name="location" value="${employee.location}" required placeholder="Location" />
								<p class="help-block">Location is required</p>
							</div>
					
					 		<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label">Email</label>
								<input type="email" class="form-control" name="email" value="${employee.email}" required placeholder="Email Address" />
								<p class="help-block">Email Address is required</p>
							</div>
												
						</div>
					</div>
				</div>
				
				<c:if test="${(formMode == 'add')}">
					<button type="button" class="btn btn-primary" onclick="saveEmployee(empForm, 'add', '${pageContext.servletContext.contextPath}/save');">Save</button>
					<button type="button" class="btn btn-link" onclick="submitAjax('cancel', '${pageContext.servletContext.contextPath}/cancel/0', '0', '${currentIndex}')">Cancel</button>	<!-- TODO: go back to the proper paginated employee page -->
				</c:if>
				<c:if test="${(formMode == 'edit')}">
					<button type="button" class="btn btn-primary" onclick="saveEmployee(empForm, 'edit', '${pageContext.servletContext.contextPath}/update');">Save</button>
					<button type="button" class="btn btn-link" onclick="submitAjax('cancel', '${pageContext.servletContext.contextPath}/cancel/${employee.id}', '${employee.id}', '${currentIndex}')">Cancel</button>	<!-- TODO: go back to the proper paginated employee page -->
				</c:if>

			</div>

			<div class="col-xs-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Phone Numbers</h3>
					</div>
					<div class="panel-body">
						<c:forEach items="${employee.numbers}" var="number" varStatus="status">
						<div class="form-group">
							<div class="num">${number.phoneId}</div>
							<input type="hidden" name="numbers[${status.index}].phoneId" value="${number.phoneId}" />
							<select name="numbers[${status.index}].phoneType">
								<option value=""></option>
								<option value="Home"
									<c:if test="${(number.phoneType == 'Home')}">
										<c:out value=' selected'/>
									</c:if>
								>Home</option>
								<option value="Mobile"
									<c:if test="${(number.phoneType == 'Mobile')}">
										<c:out value=' selected'/>
									</c:if>								
								>Mobile</option>
								<option value="Office"
									<c:if test="${(number.phoneType == 'Office')}">
										<c:out value=' selected'/>
									</c:if>	
								>Office</option>
							</select>
							<input type="text" class="form-control" name="numbers[${status.index}].phoneNumber" value="${number.phoneNumber}" required placeholder="Phone number" style="display:inline; width:50%;"/>
							<button class="removeNumber">-</button>
						</div>
						</c:forEach>
					</div>
					<div class="panel-body">
						<button class="btn btn-primary">Add new number</button>
					</div>
				</div>

			</div>

		</div>

		</form>


	</div>
</body>

</html>			
