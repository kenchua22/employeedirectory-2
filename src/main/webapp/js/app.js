function submitAjax(action, url, data, page) {
	console.log('action: ' + action);
	if (action === "delete") {
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'html',
			beforeSend: function() {
			},
			success: function(data, textStatus, xhr) {
				console.log('Redirecting to page ' + page);
				window.location = page;
			},
			error: function(xhr, textStatus, errorThrown) {
	
	        }
	    });		

	}
	
	if (action === "cancel") {
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'html',
			beforeSend: function() {
			},
			success: function(data, textStatus, xhr) {
				console.log('Redirecting to ' + data);
				window.location = data;
			},
			error: function(xhr, textStatus, errorThrown) {
	
	        }
	    });		
	}
}
function saveEmployee(form, action, url) {
	form.method = "POST";
	if (action === "edit" || action === 'add') {
		form.action = url;
		form.submit();
	}
}

function searchEmployee(url) {
	var searchTerm = $('#search').val();
	$.trim(searchTerm);
	console.log('search term >>>' + searchTerm);
	var searchPage = url + "?term=" + searchTerm + "&page=1";
	console.log('searchPage: ' + searchPage);
	window.location.href = searchPage;
}