EmployeeDirectory-1

This Employee Directory app is built with the following technologies:
Bootstrap
Spring MVC
Spring Data JPA
Hibernate
MySQL
Tomcat
Maven

Application Features:
Server-side pagination of Employee records.
Search Employees by First Name, Last Name, Title, Location, Email.

Environment Setup:
Import project into Eclipse
Execute DDL script 
Run JUnit class PopulateDBTest to insert test records.