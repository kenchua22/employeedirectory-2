<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 
<c:choose>
    <c:when test="${(searchTerm != null)}">
		<c:url var="firstUrl" value="/search?term=${searchTerm}&page=1" />
		<c:url var="lastUrl" value="/search?term=${searchTerm}&page=${page.totalPages}" />
		<c:url var="prevUrl" value="/search?term=${searchTerm}&page=${currentIndex - 1}" />
		<c:url var="nextUrl" value="/search?term=${searchTerm}&page=${currentIndex + 1}" />
    </c:when>
    <c:otherwise>
		<c:url var="firstUrl" value="/pages/1" />
		<c:url var="lastUrl" value="/pages/${page.totalPages}" />
		<c:url var="prevUrl" value="/pages/${currentIndex - 1}" />
		<c:url var="nextUrl" value="/pages/${currentIndex + 1}" />
    </c:otherwise>
</c:choose>
				        
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Employee Directory App Listings</title>

	<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootbox.js"></script>
	<script src="${pageContext.request.contextPath}/js/app.js"></script>

	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
	
</head>

<body>
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
			<p/>
			</div>
		</div>
				
		<div class="row">
			<div class="col-xs-4">
				<h4><small>Employees Count on page ${currentIndex}: ${page.numberOfElements}</small></h4>
				<h4><small>Total Employees: ${page.totalElements}</small></h4>
			</div>
			<div class="col-xs-8">
				<label for="search">Search Employee:</label>
				<input id="search" class="form-control" placeholder="Filter employees" style="width:50%;display:inline;" value="${searchTerm}"/>
				<a href="" onclick="searchEmployee('${pageContext.servletContext.contextPath}/search');return false;" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</a>
			</div>

		</div>
		<br>
		
		<%@include file="pagination.jsp"%> 
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
				
						<table class="table table-striped">
							<thead>
								<a href="${pageContext.servletContext.contextPath}/add" class="btn btn-primary btn-xs pull-right"><b>+</b>Add new Employee</a>
							    <tr>
							        <th>ID</th>
							        <th>First Name</th>
							        <th>Last Name</th>
							        <th>Title</th>
							        <th>Location</th>
							        <th>Email</th>
							        <th>Number(s)</th>
							        <th class="text-center">Action</th>
							    </tr>
							</thead>
							<c:forEach items="${page.content}" var="employee">
								<tr>
								    <td>${employee.id}</td>
								    <td>${employee.firstName}</td>
								    <td>${employee.lastName}</td>
								    <td>${employee.title}</td>
								    <td>${employee.location}</td>
								    <td>${employee.email}</td>
									<c:if test="${fn:length(employee.numbers) == 0}">
										<td></td>
									</c:if>
									<c:if test="${fn:length(employee.numbers) > 1}">
										<td><span>Multiple </span><span class="badge">${fn:length(employee.numbers)}</span></td>
									</c:if>
									<c:if test="${fn:length(employee.numbers) == 1}">
										<c:forEach var="number" items="${employee.numbers}" end="0">
											<td>${number.phoneNumber}</span></td>
										</c:forEach>
									</c:if>								    
								    <td class="text-center">
								    	<a href="${pageContext.servletContext.contextPath}/edit/${employee.id}" class='btn btn-info btn-xs' href=""><span class="glyphicon glyphicon-edit"></span> Edit</a>
								    	<a href="" class="btn btn-danger btn-xs" onclick="submitAjax('delete', '${pageContext.servletContext.contextPath}/delete/${employee.id}', '${employee.id}', '${currentIndex}')"><span class="glyphicon glyphicon-remove"></span> Del</a>
								    </td>
								</tr>
							</c:forEach>
						</table>
				
					</div>
				</div>
			</div>
		</div>
		
		<%@include file="pagination.jsp"%> 
		
	</div>
</body>

</html>			


