<!DOCTYPE html>
<html lang="en">

<head>
	<title>Employee Directory App</title>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link href="css/style.css" rel="stylesheet"/>
	
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="jumbotron">
					<h2>Employee Directory App</h2>
					<p>Spring MVC + Spring Data JPA + MySQL</p>
					<p><a href="<%=request.getContextPath()%>/pages/1" class="btn btn-primary btn-lg" role="button">Demo</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
