
/*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */
CREATE DATABASE employeedirectory_db;

USE employeedirectory_db;

CREATE TABLE EMPLOYEE (
	emp_id int(11) NOT NULL AUTO_INCREMENT,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	title varchar(200) NOT NULL,
	location varchar(1000) NOT NULL,
	email varchar(200) NOT NULL,
	modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (emp_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE EMPLOYEE_NUMBER (
  id int(11) NOT NULL AUTO_INCREMENT,
  emp_id int(11) NOT NULL,
  phone_id int(11) NOT NULL,
  phone_type char(10) NOT NULL,
  phone_number varchar(20) NOT NULL,
  modified_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

ALTER TABLE EMPLOYEE_NUMBER
ADD CONSTRAINT fk_EmpId
FOREIGN KEY (emp_id)
REFERENCES EMPLOYEE(emp_id);


